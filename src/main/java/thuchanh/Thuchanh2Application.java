package thuchanh;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import thuchanh.repository.productRepository;

@SpringBootApplication
public class Thuchanh2Application {

	public static void main(String[] args) {
		SpringApplication.run(Thuchanh2Application.class, args);
	}

}
